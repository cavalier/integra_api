<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    // Test database connection
    try {

//        $users = DB::table('users')
//            ->offset(10)
//            ->limit(5)
//            ->get();
        $data = DB::table('AWBWEB')
            ->offset(1)
            ->limit(1000)
            ->get();

//        dd($data);
//        DB::connection()->getPdo();
    } catch (\Exception $e) {
        dd($e);
        die("Could not connect to the database.  Please check your configuration. error:" . $e);
    }
    return view('welcome');
});
